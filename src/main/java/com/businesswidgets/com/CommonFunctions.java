package com.businesswidgets.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mainRunnerTest.com.MainStepDefinition;

public class CommonFunctions extends MainStepDefinition {

	
	public static void navigateURL(String url)
	{
		driver.navigate().to(url);
	}
	
	public static void sendKeys(WebDriver driver,By xpath, String name)
	{
		try
		{
			driver.findElement(xpath).sendKeys(name);
		}
		catch(Exception e)
		{
			
		}
	}
	
	
	public static boolean clickVisibilityofElement(WebDriver driver,By element, Integer time)
	{
		boolean flag=true;
		try
		{
		WebDriverWait wait=new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.visibilityOfElementLocated(element));
		driver.findElement(element).click();
		flag=true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			flag=false;
		}
		return flag;
		
		
		
	}
	
	public static boolean clickPresenceOfElement(WebDriver driver,Integer time, By element)
	{
		boolean flag=true;
		try
		{
		WebDriverWait wait=new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.presenceOfElementLocated(element));
		flag=true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			flag=false;
		}
		return flag;
		
		
	
		
	}
	
	
	
	
	public static boolean waitForElement(WebDriver driver,Integer time, By element)
	{
		boolean flag=true;
		try
		{
		WebDriverWait wait=new WebDriverWait(driver, time);
		wait.until(ExpectedConditions.presenceOfElementLocated(element));
		flag=true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			flag=false;
		}
		return flag;
		
		
	
		
	}
	
	
	
	
	
	
	
	
	
	
	
}
