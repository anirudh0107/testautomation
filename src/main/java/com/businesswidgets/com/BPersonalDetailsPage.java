package com.businesswidgets.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.or.com.PersonalDetailsPagePO;

public class BPersonalDetailsPage {
	  
	
	PersonalDetailsPagePO ppo=new PersonalDetailsPagePO();
	public void enterFirstname(WebDriver driver,String name)
	{
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(ppo.getFirstname()), 60);
		CommonFunctions.sendKeys(driver, By.xpath(ppo.getFirstname()), name);
	}
	
	public void enterLastname(WebDriver driver,String named)
	{
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(ppo.getLastname()), 60);
		CommonFunctions.sendKeys(driver, By.xpath(ppo.getLastname()), named);
	}
	
	public void enterMobilenumber(WebDriver driver,String name)
	{
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(ppo.getMobileno()), 60);
		CommonFunctions.sendKeys(driver, By.xpath(ppo.getMobileno()), name);
	}
	
	public void enterSecondaryNumber(WebDriver driver,String name)
	{
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(ppo.getSecondarymobileno()), 60);
		CommonFunctions.sendKeys(driver, By.xpath(ppo.getSecondarymobileno()), name);
	}
	
	public void enterEmailaddress(WebDriver driver,String name)
	{
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(ppo.getEmailaddress()), 60);
		CommonFunctions.sendKeys(driver, By.xpath(ppo.getEmailaddress()), name);
	}
	
	public void enterConfirmEmailaddress(WebDriver driver,String name)
	{
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(ppo.getConfirmemailaddress()), 60);
		CommonFunctions.sendKeys(driver, By.xpath(ppo.getConfirmemailaddress()), name);
	}
	

	
	public void enterCompanyName()
	{
		
	}
	
	public void enterPostcode()
	{
		
	}
	
	public void enterConfirmPostcode()
	{
		
	}
	
}