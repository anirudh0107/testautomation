package com.businesswidgets.com;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.or.com.PackagesPagePO;

public class BPackagePage {

	PackagesPagePO bpo = new PackagesPagePO();

	public void selectPackage(WebDriver driver) {
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(bpo.getPackageselection()), 60);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void selectNextinBoostPage(WebDriver driver) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement element = driver.findElement(By.xpath(bpo.getBoostNext()));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

		CommonFunctions.clickVisibilityofElement(driver, By.xpath(bpo.getBoostNext()), 60);
	}
}
