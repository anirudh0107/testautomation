package com.businesswidgets.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.or.com.HomePagePO;



public class BHomePage {
	
	HomePagePO po=new HomePagePO();
	
	public void enterPostCode(WebDriver driver,String name)
	{
		CommonFunctions.clickVisibilityofElement(driver,By.xpath(po.getPostcode()),30);
		CommonFunctions.sendKeys(driver, By.xpath(po.getPostcode()), name);
	}

	
	public void clickWantnewLine(WebDriver driver)
	{
		CommonFunctions.clickVisibilityofElement(driver,By.xpath(po.getNewline()),30);
	}
	
	public void clickFindPackages(WebDriver driver)
	{
		CommonFunctions.clickVisibilityofElement(driver,By.xpath(po.getFindpackages()),30);
	}
}

