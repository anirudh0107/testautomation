package com.businesswidgets.com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.or.com.AddressPagePO;


public class BAddressPage {
	
	AddressPagePO apo=new AddressPagePO();
	
	public void selectNewlineAddress(WebDriver driver)
	{
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(apo.getNewlineAddress()), 60);
	}
	
	public void clickNextinAddressPage(WebDriver driver)
	{
		CommonFunctions.clickVisibilityofElement(driver, By.xpath(apo.getAddressNext()), 60);
	}

}
