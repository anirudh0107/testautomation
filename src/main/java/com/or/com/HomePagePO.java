package com.or.com;

public class HomePagePO {
	
	private String postcode="//input[@name='postcode']";
	private String newline="//a[text()='Want a new line?']";
	private String findpackages="//button[contains(text(),'Find Packages')]";
	
	
	public String getPostcode() {
		return postcode;
	}
	public String getNewline() {
		return newline;
	}
	public String getFindpackages() {
		return findpackages;
	}

}
