package com.or.com;

public class PackagesPagePO {
	
	
	private String packageselection="//div[@ng-if='bbPackage']//button[text()='Buy Simply']";
	private String boostNext="//a[@class='button button--small--variant-2 pull-right']";
	public String getPackageselection() {
		return packageselection;
	}
	public String getBoostNext() {
		return boostNext;
	}

}
