package com.or.com;

public class AddressPagePO {
	
	private String newlineAddress="//div[text()='Unit 4, Westfield Road, Guildford, GU1 1SF']";
	
	private String addressNext="//div[@class='col-sm-2']/button";
	
	public String getNewlineAddress() {
		return newlineAddress;
	}
	public String getAddressNext() {
		return addressNext;
	}

}
