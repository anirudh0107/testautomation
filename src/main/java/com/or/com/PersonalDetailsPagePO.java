package com.or.com;

public class PersonalDetailsPagePO {
	
	
	private String firstname="//input[@id='firstname']";
	private String lastname="//input[@id='lastname']";
	private String mobileno="//input[@id='mobile']";
	private String secondarymobileno="//input[@id='secondary']";
	private String emailaddress="//input[@id='email']";
	private String confirmemailaddress="//input[@id='confirmemail']";
	private String companyname="//input[@id='companyEnter']";
	private String postcode="//input[@id='password']";
	private String confirmpostcode="confirmpassword";
	public String getFirstname() {
		return firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public String getMobileno() {
		return mobileno;
	}
	public String getSecondarymobileno() {
		return secondarymobileno;
	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public String getConfirmemailaddress() {
		return confirmemailaddress;
	}
	public String getCompanyname() {
		return companyname;
	}
	public String getPostcode() {
		return postcode;
	}
	public String getConfirmpostcode() {
		return confirmpostcode;
	}
	
	
	
}
