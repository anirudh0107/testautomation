package com.globaldatavariables.com;

import java.util.Properties;

public class GlobalDataVariables {
	
	
	private String fname=null;
	private String lname =null;
	private String mobileno =null;
	private String secondarymobileno =null;
	private String emailaddress =null;
	private String confirmemailaddress =null;
	
	
	public GlobalDataVariables()
	{
		
	}
	
	
	private Properties testdata=new Properties();
	
	public Properties getTestdata() {
		return testdata;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getSecondarymobileno() {
		return secondarymobileno;
	}
	public void setSecondarymobileno(String secondarymobileno) {
		this.secondarymobileno = secondarymobileno;
	}
	public String getEmailaddress() {
		return emailaddress;
	}
	public void setEmailaddress(String emailaddress) {
		this.emailaddress = emailaddress;
	}
	public String getConfirmemailaddress() {
		return confirmemailaddress;
	}
	public void setConfirmemailaddress(String confirmemailaddress) {
		this.confirmemailaddress = confirmemailaddress;
	}
	
	
	

}
