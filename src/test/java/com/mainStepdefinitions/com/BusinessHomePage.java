package com.mainStepdefinitions.com;

import org.openqa.selenium.By;

import com.businesswidgets.com.BHomePage;
import com.businesswidgets.com.CommonFunctions;
import com.mainRunnerTest.com.MainStepDefinition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class BusinessHomePage  extends MainStepDefinition {
	BHomePage home=new BHomePage();
	
	@Given("^End User access the site$")
	public void end_User_access_the_site() throws Throwable {
	    try
	    {
	    	CommonFunctions.navigateURL("https://www.talktalk.co.uk/business/?_ga=2.232932254.1847297393.1571128861-935843166.1571128861");
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	    
	}

	@When("^User is on home page$")
	public void user_is_on_home_page() throws Throwable {

		try
	    {
			home.enterPostCode(driver,"GU11SF");
		
		//	home.clickWantnewLine(driver);
		//	home.clickFindPackages(driver);
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    }
	}
	
}
