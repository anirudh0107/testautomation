package com.mainStepdefinitions.com;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeSuite;

import com.globaldatavariables.com.GlobalDataVariables;
import com.mainRunnerTest.com.MainStepDefinition;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Setup extends MainStepDefinition {

	private GlobalDataVariables globalvars;

	public Setup(GlobalDataVariables globalvars) {
		this.globalvars = globalvars;
	}

	
	@Before
	public void initiation() throws IOException {
		System.out.println("load file");
		FileInputStream fis;
		try {
			fis = new FileInputStream(new File("D:\\Mohanworkspace\\Rohan\\testdata.properties"));
			globalvars.getTestdata().load(fis);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.setProperty("webdriver.chrome.driver", "D:\\Softwares\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	@After
	public void tearDown() {
driver.quit();
	}
}
