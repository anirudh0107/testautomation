package com.mainStepdefinitions.com;

import com.businesswidgets.com.BPackagePage;
import com.mainRunnerTest.com.MainStepDefinition;

import cucumber.api.java.en.When;

public class PackagesPage  extends MainStepDefinition {

	BPackagePage pack=new BPackagePage();
	@When("^User select the required package$")
	public void user_select_the_required_package() throws Throwable {
	    try
	    {
	    	pack.selectPackage(driver);
	    	pack.selectNextinBoostPage(driver);
	    }
	    catch(Exception e)
	    {
	    	
	    }
	}
	
}
