package com.mainStepdefinitions.com;

import com.businesswidgets.com.BAddressPage;
import com.mainRunnerTest.com.MainStepDefinition;

import cucumber.api.java.en.When;

public class AddressPage extends MainStepDefinition {
	
	BAddressPage address=new BAddressPage();
	@When("^User select the address$")
	public void user_select_the_address() throws Throwable {
	   try
	   {
		   address.selectNewlineAddress(driver);
		   address.clickNextinAddressPage(driver);
	   }
	   catch(Exception e)
	   {
		   e.printStackTrace();
	   }
	}

}
