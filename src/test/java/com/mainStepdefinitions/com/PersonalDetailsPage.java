package com.mainStepdefinitions.com;

import com.businesswidgets.com.BPersonalDetailsPage;
import com.globaldatavariables.com.GlobalDataVariables;
import com.mainRunnerTest.com.MainStepDefinition;

import cucumber.api.java.en.When;

public class PersonalDetailsPage  extends MainStepDefinition {
	
	private GlobalDataVariables globalvars;
	
	public PersonalDetailsPage(GlobalDataVariables globalvars)
	{
		this.globalvars=globalvars;
	}

	GlobalDataVariables bsv=new GlobalDataVariables();
	BPersonalDetailsPage bpo=new BPersonalDetailsPage();
	
	@When("^Enter the required personal details$")
	public void enter_the_required_personal_details() throws Throwable {
	   
		try {
			
			globalvars.setFname(globalvars.getTestdata().getProperty("Firstname"));
			globalvars.setLname(globalvars.getTestdata().getProperty("Lastname"));
			globalvars.setMobileno(globalvars.getTestdata().getProperty("Mobilenmber"));
			globalvars.setSecondarymobileno(globalvars.getTestdata().getProperty("Secondarymobilenumber"));
			globalvars.setEmailaddress(globalvars.getTestdata().getProperty("Emailaddress"));
			System.out.println(globalvars.getFname());
			
			bpo.enterFirstname(driver,globalvars.getFname());
			bpo.enterLastname(driver, "mohan");
			bpo.enterMobilenumber(driver, "0718918912");
			bpo.enterSecondaryNumber(driver, "0718918912");
			bpo.enterEmailaddress(driver, "test@gmail.com");
			bpo.enterConfirmEmailaddress(driver, "test@gmail.com");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
}
